<!DOCTYPE html>
<html lang="en">
<head>
    <title>Truong Gia</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('/css/home.css')}}">
    <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/animation/animate.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="{{asset('/js/home.js')}}"></script>
</head>
<body>
<header class="nav-wrapper">
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" href="#">TruongGia</a>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
        </ul>
    </nav>
</header>
<section class="landing parallax text-center">

    <div class="avatar">
        <img src="https://www.google.com.vn/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png">
    </div>
    <h2>Truong Gia</h2>
    <h3 class="sub-title">Nullam porttitor porta augue vel iaculis. Pellentesque a pretium erat. Maecenas semper laoreet
        dapibus et quatre malesuada.</h3>

</section>
<section class="event">
    <div class="container">
        <h4 class="animated jackInTheBox">Hi There! I'm Truong Gia</h4>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <p>I am a professional web designer from Manchester, England. I create beautiful professional websites
                    using best practice accessibility. I enjoy turning complex problems into simple, beautiful and
                    intuitive interface designs.</p>

                <div class="row">
                    <div class="col-md-6">
                        <p><i class="fa fa-envelope" aria-hidden="true"></i> <strong>Email:</strong>
                            truongnx081294@gmail.com</p>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> <strong>Location:</strong> Tốt Động,
                            Chương Mỹ, Hà Nội</p>
                    </div>

                    <div class="col-md-6">
                        <p><i class="fa fa-calendar" aria-hidden="true"></i> <strong>DOB:</strong> 08 - 12 -1994</p>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> <strong>Phone:</strong> 0972.243.913</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">

                <p class="title-progress">HTML5, CSS3 <span>90%</span></p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                         aria-valuemax="100" style="width:70%">
                        <span class="sr-only">70% Complete</span>
                    </div>
                </div>


                <p class="title-progress">HTML5, CSS3 <span>90%</span></p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                         aria-valuemax="100" style="width:70%">
                        <span class="sr-only">70% Complete</span>
                    </div>
                </div>


                <p class="title-progress">HTML5, CSS3 <span>90%</span></p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                         aria-valuemax="100" style="width:70%">
                        <span class="sr-only">70% Complete</span>
                    </div>
                </div>


                <p class="title-progress">HTML5, CSS3 <span>90%</span></p>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                         aria-valuemax="100" style="width:70%">
                        <span class="sr-only">70% Complete</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="odd text-center">
    <h2>My Services</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <p><i class="fa fa-laptop fa-3x" aria-hidden="true"></i></p>
                </h2>
                <h4>ABCasdasdasd</h4>
                <p>Nullam porttitor porta augue vel iaculis. Pellentesque a pretium erat. Maecenas semper laoreet
                    dapibus et quatre malesuada.</p>
            </div>

            <div class="col-md-4 col-xs-12 center-bordered">
                <p><i class="fa fa-laptop fa-3x" aria-hidden="true"></i></p>
                </h2>
                <h4>ABCasdasdasd</h4>
                <p>Nullam porttitor porta augue vel iaculis. Pellentesque a pretium erat. Maecenas semper laoreet
                    dapibus et quatre malesuada.</p>
            </div>

            <div class="col-md-4 col-xs-12">
                <p><i class="fa fa-laptop fa-3x" aria-hidden="true"></i></p>
                </h2>
                <h4>ABCasdasdasd</h4>
                <p>Nullam porttitor porta augue vel iaculis. Pellentesque a pretium erat. Maecenas semper laoreet
                    dapibus et quatre malesuada.</p>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-4 col-xs-12">
                <p><i class="fa fa-laptop fa-3x" aria-hidden="true"></i></p>
                </h2>
                <h4>ABCasdasdasd</h4>
                <p>Nullam porttitor porta augue vel iaculis. Pellentesque a pretium erat. Maecenas semper laoreet
                    dapibus et quatre malesuada.</p>
            </div>

            <div class="col-md-4 col-xs-12 center-bordered">
                <p><i class="fa fa-laptop fa-3x" aria-hidden="true"></i></p>
                </h2>
                <h4>ABCasdasdasd</h4>
                <p>Nullam porttitor porta augue vel iaculis. Pellentesque a pretium erat. Maecenas semper laoreet
                    dapibus et quatre malesuada.</p>
            </div>

            <div class="col-md-4 col-xs-12">
                <p><i class="fa fa-laptop fa-3x" aria-hidden="true"></i></p>
                </h2>
                <h4>ABCasdasdasd</h4>
                <p>Nullam porttitor porta augue vel iaculis. Pellentesque a pretium erat. Maecenas semper laoreet
                    dapibus et quatre malesuada.</p>
            </div>
        </div>
    </div>
</section>

<section class="event text-center">
    <div class="container">
        <h2>Contact me</h2>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="contact-item">
                    <div class="icon-contact-wrapper"><i class="fa fa-phone fa-2x"></i></div>
                    <div class="info-contact">
                        <h6>PHONE</h6>
                        <p>0972.234.913</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="contact-item">
                    <span class="icon-contact-wrapper"><i class="fa fa-phone fa-2x"></i></span>
                    <div class="info-contact">
                        <h6>PHONE</h6>
                        <p>0972.234.913</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="contact-item">
                    <div class="icon-contact-wrapper"><i class="fa fa-phone fa-2x"></i></div>
                    <div class="info-contact">
                        <h6>PHONE</h6>
                        <p>0972.234.913</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <input type="text" name="name" id="name" class="input-field" required="required" placeholder="Name">
            </div>
            <div class="col-md-6 col-xs-12">
                <input type="text" name="name" id="name" class="input-field" required="required" placeholder="Name">
            </div>
        </div>
        <div>
            <textarea name="message" id="message" class="input-field" required="required" placeholder="Message"></textarea>
        </div>
    </div>
</section>

<section class="odd text-center">
    <h3>Truong Gia</h3>
    <ul class="contact-footer">
        <li>
            <a href="#"><i class="fa fa-facebook fa-1x" aria-hidden="true"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-twitter fa-1x" aria-hidden="true"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-behance fa-1x" aria-hidden="true"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-linkedin fa-1x" aria-hidden="true"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-instagram fa-1x" aria-hidden="true"></i></a>
        </li>
    </ul>
    <p class="text-copyright">TruongGia © 2018. All right reserved, designed by TruongGia</p>
</section>

</body>
</html>


